#!usr/bin/env python

import trollstoiAI
import logging, sys
from telegram import Update
from telegram.ext import Updater, CommandHandler, CallbackContext
import time 

multiplier = 5000
#Enable logging
"""
logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
        )

logger = logging.getLogger(__name__)
"""
troll_person = "Niemand"
troll_date = "No Date"
#Comandhandlers

def start(update: Update, context: CallbackContext) -> None:
    update.message.reply_text('Hello my fellow Trolls, I am Leo Trollstoy. Start the game with /startgame. How to play? As simple as /rules. Also, i will share my wisdom with /quote')


def alarm(context):
    job = context.job
    global troll_person
    global troll_date
    if (int(time.strftime("%H",time.localtime())) > 8) & (int(time.strftime("%H",time.localtime())) < 21):
        if trollstoiAI.endgame():
            text = "The Game is over\nI should be at " + troll_person + "´s room since " + troll_date 
            context.bot.send_message(job.context, text=text)
            job_removed = remove_job_if_exists(str(job.context),context)
        else:
            text = trollstoiAI.myquote()
            reset_timer(context)
            if context.job_queue.get_jobs_by_name(str(job.context)):
                context.bot.send_message(job.context,text=text)
            else:
                context.bot.send_message(job.context,"I dont feel good...")


def remove_job_if_exists(name,context):
    current_jobs = context.job_queue.get_jobs_by_name(name)
    if not current_jobs:
        return False
    for job in current_jobs:
        job.schedule_removal()
    return True


def set_timer(update: Update, context: CallbackContext) -> None:
    chat_id = update.message.chat_id
    global multiplier
    try:
        due = trollstoiAI.endgame_interval()*multiplier
        if due < 0:
            update.message.reply_text("Sorry, this is just impossible for a troll like me..")
            return
        job_removed = remove_job_if_exists(str(chat_id),context)
        context.job_queue.run_repeating(alarm,due,context=chat_id,name=str(chat_id))

        text = "The game starts now!"
        if job_removed:
            text += " ... again."
        update.message.reply_text(text)

    except (IndexError,ValueError):
        update.message.reply_text("Usage: /set <hours>")

def reset_timer(context):
    chat_id = context.job.context
    global multiplier

    due = trollstoiAI.endgame_interval()*multiplier
    job_removed = remove_job_if_exists(str(chat_id),context)
    context.job_queue.run_repeating(alarm,due,context=chat_id,name=str(chat_id))

def unset(update: Update, context: CallbackContext) -> None:
    chat_id = update.message.chat_id
    job_removed = remove_job_if_exists(str(chat_id),context)
    text = "Timer successfully canceled" if job_removed else "You have no active timer"
    update.message.reply_text(text)

def trolling_person(update: Update, context: CallbackContext):
    chat_id = update.message.chat_id
    global troll_person
    global troll_date
    try:
        troll_person = str(context.args[0])
        troll_date = time.strftime("%c",time.localtime())
        update.message.reply_text(troll_person + " is now trolled")
    except (IndexError,ValueError):
        update.message.reply_text("Usage: /troll <person>")

def get_rules(update: Update, context:CallbackContext):
    text = "The rules are simple:n\0. I have to STAND on my feet. Or placed in some kind of intentional position. Just throwing me is not very kind. I have trollfeelings too.\n1. Only put me in a room if the roomowner is inside. \n2. You have to leave the room before the owner notices my appearance. \n3. The game will stop when i say so! \nIt is save to say, that it will not end till " + trollstoiAI.get_endgame_begin() + ". \nBut from then on, i will decide now and then if the game is over.\n" + trollstoiAI.get_endgame_final() + ",the game will end at last!\n\n Hot Tip: If you troll someone, let me know with \n/troll <personname>, but PSSST:\nLet me know in private so we only find out in the end where I am."
    update.message.reply_text(text)

def getquote(update: Update, context: CallbackContext):
    chat_id = update.message.chat_id
    update.message.reply_text("Hm let me think...")
    text = trollstoiAI.myquote()
    update.message.reply_text(text)

def main(telegram_token):
    updater = Updater(telegram_token,use_context=True)

    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler("start",start))
    dispatcher.add_handler(CommandHandler("help", start))
    dispatcher.add_handler(CommandHandler("startgame", set_timer))
    dispatcher.add_handler(CommandHandler("unset", unset))
    dispatcher.add_handler(CommandHandler("quote", getquote))
    dispatcher.add_handler(CommandHandler("troll", trolling_person))
    dispatcher.add_handler(CommandHandler("rules", get_rules))

    updater.start_polling()

    updater.idle()


if __name__ == "__main__":
    telegram_token = str(sys.argv[1])

    main(telegram_token)
