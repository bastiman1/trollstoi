import time, random, requests, json
import pandas
from math import exp, floor
from quote import quote

endgame_window_begin = time.strptime("2020-11-08 00:00:00","%Y-%m-%d %H:%M:%S")
endgame_window_final = time.strptime("2020-12-10 00:00:00","%Y-%m-%d %H:%M:%S")

def endgame_interval():
    return floor(random.random() * 8)+2

def get_endgame_begin():
    global endgame_window_begin
    return(time.strftime("%c",endgame_window_begin))

def get_endgame_final():
    global endgame_window_final
    return(time.strftime("%c",endgame_window_final))

def endgame():
    timenow = time.gmtime()
    if timenow > endgame_window_final:
        print("Endgame final reached")
        return(True)
    elif timenow > endgame_window_begin:
        print("Endgame started")
        return(rolldice())
    else:
        print("Not in endgame yet")
        return(False)

def rolldice():
    print("rolling dice")
    endgame_window_size = float(time.strftime("%s",endgame_window_final)) - float(time.strftime("%s",endgame_window_begin))
    pos = time.time()
    endgame_window_progress = (pos - float(time.strftime("%s",endgame_window_begin))) / (float(time.strftime("%s",endgame_window_final)) - float(time.strftime("%s",endgame_window_begin)))
    print("progress is:")
    print(endgame_window_progress)
    sig_progress = 1/(1+exp(-(endgame_window_progress-0.5)*10))
    print("sig_progress is:")
    print(sig_progress)

    rand = random.random()
    print("rand is:")
    print(rand)
    if sig_progress > rand:
        return(True)
    else:
        return(False)


def myquote():
    search = "Leo Tolstoy"
    try: 

        x = quote(search, limit = 100)
    
        quote_count = len(x)
        random_quote = floor(random.random()*quote_count)-1
        thisquote = x[random_quote]["quote"]
        return trollify(thisquote)
    except:
        return "Game is still going"

def trollify(text):
    try:
        dic = pandas.read_csv("dict.csv",delimiter=";")
        for i in range(0,len(dic.english)):
            text = text.replace(str(dic.english[i]),str(dic.trollish[i]))
        return text
    except:
        return text
